include("./miniep8.jl")

using Test

function test()
    @test compareByValue("2♠", "A♠") == true
    @test compareByValue("K♥", "10♥") == false
    @test compareByValue("10♠", "10♥") == false

    @test compareByValueAndSuit("2♠", "A♠") == true
    @test compareByValueAndSuit("K♥", "10♥") == false
    @test compareByValueAndSuit("10♠", "10♥") == true
    @test compareByValueAndSuit("A♠", "2♥") == true

    @test insercao(["10♥", "10♦", "K♠", "A♠", "J♠", "A♠"]) ==
                   ["10♦", "J♠", "K♠", "A♠", "A♠", "10♥"]


    println("Tests passed!")
end

test()
