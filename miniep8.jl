function compareByValue(x, y)
    # Splits the strings keeping the delimiter
    x = split(x, r"(?=[♠♥♦♣])")
    y = split(y, r"(?=[♠♥♦♣])")

    val_x = cardValueToInt(x[1])
    val_y = cardValueToInt(y[1])

    return val_x < val_y
end

function cardValueToInt(value)
    try
        return parse(Int, value)
    catch
        value == "J" && return 11
        value == "Q" && return 12
        value == "K" && return 13
        value == "A" && return 14

        return 0
    end
end

function compareByValueAndSuit(x, y)
    # Splits the strings keeping the delimiter
    x = split(x, r"(?=[♠♥♦♣])")
    y = split(y, r"(?=[♠♥♦♣])")

    suit_x = cardSuitToInt(x[2])
    suit_y = cardSuitToInt(y[2])
    val_x = cardValueToInt(x[1])
    val_y = cardValueToInt(y[1])

    if suit_x != suit_y
        return suit_x < suit_y
    else
        return val_x < val_y
    end
end

function cardSuitToInt(suit)
    suit == "♦" && return 1
    suit == "♠" && return 2
    suit == "♥" && return 3
    suit == "♣" && return 4

    return 0
end

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j], v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

